// Main scripts for the project
! function ($) {
  //Variables
  var mainPlatform, primaryIndex, modifiedTicketsValue, modifiedPriceValue, paymentProcessingIncluded;
  let eventbrite, ticketTailor, universe, brownPaperTickets, ticketSource, billetto, tiTo, skiddle, eventix, xingEvents;
  var ticketsHandle = $('.tickets-handle');
  var priceHandle = $('.price-handle');
  var priceRange = $('.price-range');
  var ticketsRange = $('.tickets-range');
  var currencyName = 'USD';
  var currencySymbols = {
    'USD': '$',
    'EUR': '€',
    'GBP': '£',
    'CAD': '$',
    'AUD': '$'
  }
  var countryName = {
    'USD': 'US',
    'EUR': 'Europe',
    'GBP': 'UK',
    'CAD': 'Canada',
    'AUD': 'Australia'
  }
  var platformLink = {
    'Eventbrite': 'https://www.eventbrite.com',
    'Ticket Tailor': 'https://www.tickettailor.com/?rf=tebsite',
    'Universe': 'https://www.universe.com',
    'Brown Paper Tickets': 'https://www.brownpapertickets.com',
    'Ticket Source': 'https://www.ticketsource.co.uk',
    'Billetto': 'https://billetto.co.uk',
    'tiTo': 'https://ti.to/home',
    'Skiddle': 'https://www.skiddle.com',
    'Eventix': 'https://eventix.io',
    'Xing Events': 'https://www.xing-events.com'
  }
  //Pros-Cons
  var prosObject = {
    'Eventbrite': ['Popular brand name', 'Customer facing discovery platform'],
    'Ticket Tailor': ['Exceptional customer support', 'Feature rich and easy to use', 'Instant payout and reliable platform'],
    'Universe': ['Nicely designed interface'],
    'Brown Paper Tickets': ['Fair trade company values', 'Physical tickets supported'],
    'Ticket Source': ['UK discovery supported', 'Customer box office available'],
    'Billetto': ['Good customer support', 'Discovery page for European events'],
    'Ti.to': ['Nice user interface and simple to use', 'Generous charity discount'],
    'Skiddle': ['Very good for UK music discovery', 'Nice interface and design'],
    'Eventix': ['Easy to use', 'One of the cheaper options'],
    'Xing Events': ['Lots of additional features/plugins', 'Good local option for European organisers']
  }
  var consObject = {
    'Eventbrite': ['One of the most expensive platforms', 'Eventbrite\'s Payment processor is 4-5 days after event'],
    'Ticket Tailor': ['No discovery platform'],
    'Universe': ['One of the most expensive platforms', 'No marketing included in standard price'],
    'Brown Paper Tickets': ['Outdated technology and user interface', 'Pay out for organiser after event', 'Very US focussed'],
    'Ticket Source': ['Outdated technology and user interface', 'Standard payout is after event'],
    'Billetto': ['Payouts are after the event', 'Various complaints about customer checkout flow'],
    'Ti.to': ['Limited amount of online reviews', 'High percentage per ticket'],
    'Skiddle': ['Very expensive fees', 'UK only'],
    'Eventix': ['Outdated technology and interface'],
    'Xing Events': ['Complicated to use', 'Expensive fees']
  }
  //Reviews
  var capterraReviews = {
    'Eventbrite': {
      'rating': 4.6,
      'quantity': 2321
    },
    'Ticket Tailor': {
      'rating': 4.8,
      'quantity': 316
    },
    'Universe': {
      'rating': 4.4,
      'quantity': 33
    },
    'Brown Paper Tickets': {
      'rating': 4.4,
      'quantity': 16
    },
    'Ticket Source': {
      'rating': 4.7,
      'quantity': 311
    },
    'Billetto': {
      'rating': 'N/A',
      'quantity': 'N/A'
    },
    'Ti.to': {
      'rating': 'N/A',
      'quantity': 'N/A'
    },
    'Skiddle': {
      'rating': 'N/A',
      'quantity': 'N/A'
    },
    'Eventix': {
      'rating': 4.6,
      'quantity': 830
    },
    'Xing Events': {
      'rating': 3.3,
      'quantity': 3
    }
  }
  var g2Crowd = {
    'Eventbrite': {
      'rating': 4.4,
      'quantity': 609
    },
    'Ticket Tailor': {
      'rating': 4.9,
      'quantity': 71
    },
    'Universe': {
      'rating': 4.5,
      'quantity': 25
    },
    'Brown Paper Tickets': {
      'rating': 4.1,
      'quantity': 29
    },
    'Ticket Source': {
      'rating': 4.8,
      'quantity': 10
    },
    'Billetto': {
      'rating': 'N/A',
      'quantity': 'N/A'
    },
    'Ti.to': {
      'rating': 'N/A',
      'quantity': 'N/A'
    },
    'Skiddle': {
      'rating': 4.9,
      'quantity': 60
    },
    'Eventix': {
      'rating': 4.7,
      'quantity': 117
    },
    'Xing Events': {
      'rating': 'N/A',
      'quantity': 'N/A'
    }
  }
  var trustpilot = {
    'Eventbrite': {
      'rating': 1.4,
      'quantity': 129
    },
    'Ticket Tailor': {
      'rating': 4.9,
      'quantity': 97
    },
    'Universe': {
      'rating': 2.8,
      'quantity': 4
    },
    'Brown Paper Tickets': {
      'rating': 'N/A',
      'quantity': 'N/A'
    },
    'Ticket Source': {
      'rating': 4.5,
      'quantity': 158
    },
    'Billetto': {
      'rating': 4.3,
      'quantity': 312
    },
    'Ti.to': {
      'rating': 'N/A',
      'quantity': 'N/A'
    },
    'Skiddle': {
      'rating': 4.6,
      'quantity': 4752
    },
    'Eventix': {
      'rating': 'N/A',
      'quantity': 'N/A'
    },
    'Xing Events': {
      'rating': 'N/A',
      'quantity': 'N/A'
    }
  }
  //Functions
  function Platform(name, handle, percentageFee, perTicketFee, feeCap, paymentProcessor) {
    this.name = name;
    this.handle = handle;
    this.percentageFee = percentageFee;
    this.perTicketFee = perTicketFee;
    this.feeCap = feeCap;
    this.paymentProcessor = paymentProcessor;

    this.calculateAndUpdateServiceFees = function (numberOfTickets, costOfTickets) {
      var costPerTicket = (costOfTickets * this.percentageFee) + this.perTicketFee;
      if (costPerTicket > this.feeCap) {
        var costPerTicket = this.feeCap;
      }
      var serviceFees = numberOfTickets * costPerTicket;
      return serviceFees;
    }

    this.calculateAndUpdatePaymentFees = function (numberOfTickets, costOfTickets) {
      var paymentProcessingFees = this.paymentProcessor.calculateAndUpdateFees(numberOfTickets, costOfTickets);
      return paymentProcessingFees;
    }
  }

  function PaymentProcessor(name, percentageFee, perTransactionFee) {
    this.name = name;
    this.percentageFee = percentageFee;
    this.perTransactionFee = perTransactionFee;

    this.calculateAndUpdateFees = function (numberOfTickets, costOfTickets) {
      var paymentProcessingFees = (costOfTickets * this.percentageFee + this.perTransactionFee) * numberOfTickets;

      if (this.percentageFee === 0 && this.perTransactionFee == 0) {
        paymentProcessingFees = 'N/A';
      } else {
        if (costOfTickets * 1 === 0) {
          paymentProcessingFees = 0;
        }
      }
      return paymentProcessingFees;
    }
  }

  function move(arr, old_index, new_index) {
    while (old_index < 0) {
      old_index += arr.length;
    }
    while (new_index < 0) {
      new_index += arr.length;
    }
    if (new_index >= arr.length) {
      let k = new_index - arr.length;
      while ((k--) + 1) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
  }

  function showResults(a) {
    var tableSubHeader = '<th></th><th>Platform to compare</th>';
    var tableHeader = '<th></th>';
    var tableServiceFee = '<td>Ticket fees (excl. local taxes)</td>';
    var tablePaymentFee = '<td>Payment processing fees<span>Default provider</span></td>';
    var tableTotalFee = '<td>Total cost</td>';
    var reviewsFirstRow = '<td>Capterra</td>';
    var reviewsSecondRow = '<td>G2 Crowd</td>';
    var reviewsThirdRow = '<td>Trustpilot</td>';
    var prosFirstRow = '<td rowspan="2"><p class="pros-cons-col">' +
      '<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">' +
      '<path fill-rule="evenodd" clip-rule="evenodd" d="M5.5 4.5V1H4.5V4.5H1V5.5H4.5V9H5.5V5.5H9V4.5H5.5Z" fill="#20E3B5" />' +
      '<path d="M5.5 1H6V0.5H5.5V1ZM5.5 4.5H5V5H5.5V4.5ZM4.5 1V0.5H4V1H4.5ZM4.5 4.5V5H5V4.5H4.5ZM1 4.5V4H0.5V4.5H1ZM1 5.5H0.5V6H1V5.5ZM4.5 5.5H5V5H4.5V5.5ZM4.5 9H4V9.5H4.5V9ZM5.5 9V9.5H6V9H5.5ZM5.5 5.5V5H5V5.5H5.5ZM9 5.5V6H9.5V5.5H9ZM9 4.5H9.5V4H9V4.5ZM5 1V4.5H6V1H5ZM4.5 1.5H5.5V0.5H4.5V1.5ZM5 4.5V1H4V4.5H5ZM1 5H4.5V4H1V5ZM1.5 5.5V4.5H0.5V5.5H1.5ZM4.5 5H1V6H4.5V5ZM5 9V5.5H4V9H5ZM5.5 8.5H4.5V9.5H5.5V8.5ZM5 5.5V9H6V5.5H5ZM9 5H5.5V6H9V5ZM8.5 4.5V5.5H9.5V4.5H8.5ZM5.5 5H9V4H5.5V5Z" fill="#20E3B5" />' +
      '</svg>Pros' +
      '</p></td>';
    var prosLastRow = '<td style="display: none;"></td>';
    var consLastRow = '<td style="display: none;"></td>';
    var consFirstRow = '<td rowspan="2"><p class="pros-cons-col">' +
      '<svg width="10" height="3" viewBox="0 0 10 3" fill="none" xmlns="http://www.w3.org/2000/svg">' +
      '<path d="M9 2H1V1H9V2Z" fill="#EB5757" stroke="#EB5757"/>' +
      '</svg>Cons' +
      '</p></td>';
    var savingsFirstRow = '<td colspan="2" rowspan="2"><p class="savings-col">Savings</p></td>';
    var savingsLastRow = '<td style="display: none;"></td>';
    for (var i = 0; i < a.length; i++) {
      //Header
      tableHeader += '<th><a target="_blank" rel="noopener noreferrer" href="' + a[i].link + '">' + a[i].name + '</a></th>';
      var j = i + 1;
      if (j >= a.length) {} else {
        tableSubHeader += '<th># ' + j + ' Alternative</th>';
      }
      //Payment
      tableServiceFee += '<td>' + currencySymbols[currencyName] + numberWithCommas(a[i].service) + '</td>';
      if (a[i].payment === 'N/A') {
        tablePaymentFee += '<td>' + a[i].payment + '</td>';
      } else {
        tablePaymentFee += '<td>' + currencySymbols[currencyName] + numberWithCommas(a[i].payment) + '</td>';
      }
      tableTotalFee += '<td>' + currencySymbols[currencyName] + numberWithCommas(a[i].total) + '</td>';
      //Reviews
      reviewsFirstRow += '<td><div class="review-stars">' + getRatingTemplate(capterraReviews[a[i].name].rating) + '<span class="review-stars__number">' + checkRatingValue(capterraReviews[a[i].name].quantity) + '</span></div></td>';
      reviewsSecondRow += '<td><div class="review-stars">' + getRatingTemplate(g2Crowd[a[i].name].rating) + '<span class="review-stars__number">' + checkRatingValue(g2Crowd[a[i].name].quantity) + '</span></div></td>';
      reviewsThirdRow += '<td><div class="review-stars">' + getRatingTemplate(trustpilot[a[i].name].rating) + '<span class="review-stars__number">' + checkRatingValue(trustpilot[a[i].name].quantity) + '</span></div></td>';
      //Pros-Cons
      prosFirstRow += '<td>' + checkProsConsValue(prosObject[a[i].name][0]) + '</td>';
      prosLastRow += '<td>' + checkProsConsValue(prosObject[a[i].name][1]) + '</td>';
      consFirstRow += '<td>' + checkProsConsValue(consObject[a[i].name][0]) + '</td>';
      consLastRow += '<td>' + checkProsConsValue(consObject[a[i].name][1]) + '</td>';
      //Savings
      if (i === 0) {} else {
        savingsFirstRow += '<td>' + getSavings(currencySymbols[currencyName], a[0].total, a[i].total) + '</td>';
        // savingsLastRow += '<td>' + getGift(a[0].total, a[i].total) + '</td>';
      }
    }
    $('.table-section__title').html('Top 8 alternatives to ' + mainPlatform + ' in ' + countryName[currencyName]);
    $('#results-table').find('thead tr:first-child').html(tableSubHeader);
    $('#results-table').find('thead tr:last-child').html(tableHeader);
    $('#results-table .service-fee').html(tableServiceFee);
    $('#results-table .payment-fee').html(tablePaymentFee);
    $('#results-table .total-fee').html(tableTotalFee);
    $('#results-table .reviews-row.first').html(reviewsFirstRow);
    $('#results-table .reviews-row.first + tr').html(reviewsSecondRow);
    $('#results-table .reviews-row.last').html(reviewsThirdRow);
    $('#results-table .pros-row.first').html(prosFirstRow);
    $('#results-table .pros-row.last').html(prosLastRow);
    $('#results-table .cons-row.first').html(consFirstRow);
    $('#results-table .cons-row.last').html(consLastRow);
    $('#results-table .savings-row.first').html(savingsFirstRow);
    // $('#results-table .savings-row.last').html(savingsLastRow);
    setTimeout(function () {
      setRating();
    }, 500);
  }

  function formatNicely(amount) {
    return amount.toFixed(0).toLocaleString();
  }

  function getSavings(currency, primaryTotal, currentTotal) {
    var savingsValue = primaryTotal - currentTotal;
    if (savingsValue > 0) {
      return currency + savingsValue + '(' + Math.round(100 - (currentTotal / primaryTotal) * 100) + '%)';
    } else {
      return '-';
    }
  }

  function checkProsConsValue(value) {
    if (value === undefined) {
      return '-';
    } else {
      return value;
    }
  }

  function checkRatingValue(ratingValue) {
    if (ratingValue === undefined) {
      return 'N/A';
    } else {
      return ratingValue;
    }
  }

  function getGift(primaryTotal, currentTotal) {
    var donutPrice = 1;
    if ((primaryTotal - currentTotal) > 0) {
      return (primaryTotal - currentTotal) / donutPrice + ' donuts';
    } else {
      return '-';
    }
  }

  function setRating() {
    $('.rating').each(function () {
      var ratingValue = $(this).data('rating');
      $(this).find('input[value="' + ratingValue + '"]').prop('checked', true);
    });
  }

  function getRatingTemplate(ratingValue) {
    var starsHtmlTemplate = '<fieldset class="rating" data-rating="' + Math.round(ratingValue) + '">' +
      '<input type="checkbox" name="rating" value="5" /><label class = "full" title="Awesome - 5 stars"></label>' +
      '<input type="checkbox" name="rating" value="4 and a half" /><label class="half" title="Pretty good - 4.5 stars"></label>' +
      '<input type="checkbox" name="rating" value="4" /><label class = "full" title="Pretty good - 4 stars"></label>' +
      '<input type="checkbox" name="rating" value="3 and a half" /><label class="half" title="Meh - 3.5 stars"></label>' +
      '<input type="checkbox" name="rating" value="3" /><label class = "full" title="Meh - 3 stars"></label>' +
      '<input type="checkbox" name="rating" value="2 and a half" /><label class="half" title="Kinda bad - 2.5 stars"></label>' +
      '<input type="checkbox" name="rating" value="2" /><label class = "full" title="Kinda bad - 2 stars"></label>' +
      '<input type="checkbox" name="rating" value="1 and a half" /><label class="half" title="Meh - 1.5 stars"></label>' +
      '<input type="checkbox" name="rating" value="1" /><label class = "full" title="Sucks big time - 1 star"></label>' +
      '<input type="checkbox" name="rating" value="half" /><label class="half" title="Sucks big time - 0.5 stars"></label>' +
      '</fieldset>';
    return starsHtmlTemplate;
  }

  function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  }

  function getCurrencySymbol(currencyObj, currencyName) {
    if (currencyName in currencyObj) {
      return currencyObj[currencyName];
    }
  }

  function convertCurrency(value, fromCurrency, toCurrency) {
    return value * 0.3;
  }

  function calculateFee(priceValue, currency, numberOfTicketsValue, platformName) {
    var resultsArray = [];
    let platforms = [eventbrite, ticketTailor, universe, brownPaperTickets, ticketSource, billetto, tiTo, skiddle, eventix, xingEvents];
    currency = currency.toUpperCase() + '';
    numberOfTicketsValue = numberOfTicketsValue.replace(',', '');
    numberOfTicketsValue = +numberOfTicketsValue;
    for (var i = 0; i < platforms.length; i++) {
      var platformData = {};
      var platformObj = platforms[i][currency];
      var serviceFee = platformObj.calculateAndUpdateServiceFees(numberOfTicketsValue, priceValue);
      var paymentFee = platformObj.calculateAndUpdatePaymentFees(numberOfTicketsValue, priceValue);
      platformData.name = platformObj.name;
      if (paymentFee === 'N/A') {
        var totalFee = formatNicely(serviceFee);
        platformData.payment = paymentFee;
        platformData.service = formatNicely(serviceFee);
        platformData.total = totalFee;
      } else {
        var totalFee = serviceFee + paymentFee;
        platformData.payment = formatNicely(paymentFee);
        platformData.service = formatNicely(serviceFee);
        platformData.total = formatNicely(totalFee);
      }
      platformData.link = platformLink[platformObj.name];
      resultsArray[i] = platformData;
    }
    setTimeout(function () {
      $('.math-form__btn').css('opacity', 1);
      $('html, body').animate({
        scrollTop: $('.table-section').offset().top
      }, 500);
      if ($('.table-section__grid').hasClass('table-section__grid_opened')) {} else {
        $('.table-section__grid').addClass('table-section__grid_opened');
      }
    }, 500);
    return resultsArray;
  }

  function changeCurrencySymbol(sign) {
    $('.currency-symbol').html(sign);
    var stringFirstChar = priceHandle.data('value').charAt(0);
    var modifiedDataValue = priceHandle.data('value').replace(stringFirstChar, sign);
    priceHandle.attr('data-value', modifiedDataValue);
    return;
  }

  function getRangeSliderInstance(rangeSlider) {
    return rangeSlider.slider('instance');
  }

  function convertCurrencyAsynch(value, from, to) {
    return $.ajax({
      url: ajax.url,
      data: {
        'action': 'currency',
        'value': value,
        'from': from,
        'to': to
      },
      type: 'POST',
      beforeSend: function () {
        $('.math-form__btn').css('opacity', .4);
      },
      success: function () {
        $('.math-form__btn').css('opacity', 1);
      }
    });
  }
  $(document).ready(function () {
    //Get converted currency
    $.when(
      convertCurrencyAsynch(0.99, 'USD', 'AUD'), //usdToAud
      convertCurrencyAsynch(0.6, 'EUR', 'USD'), //eurToUsd
      convertCurrencyAsynch(0.6, 'EUR', 'CAD'), //eurToCad
      convertCurrencyAsynch(0.6, 'EUR', 'AUD'), //eurToAud
      convertCurrencyAsynch(0.25, 'GBP', 'USD'), //gbpToUsd
      convertCurrencyAsynch(0.25, 'GBP', 'EUR'), //gbpToEur
      convertCurrencyAsynch(0.25, 'GBP', 'CAD'), //gbpToCad
      convertCurrencyAsynch(0.25, 'GBP', 'AUD'), //gbpToAud
      convertCurrencyAsynch(1, 'EUR', 'GBP'), //eurToGbp
      convertCurrencyAsynch(1, 'EUR', 'USD'), //eurToUsd2
      convertCurrencyAsynch(1, 'EUR', 'CAD'), //eurToCad2
      convertCurrencyAsynch(1, 'EUR', 'AUD'), //eurToAud2
      convertCurrencyAsynch(0.99, 'EUR', 'CAD'), //eurToCad3
      convertCurrencyAsynch(0.99, 'EUR', 'AUD') //eurToAud3
    ).then((usdToAud, eurToUsd, eurToCad, eurToAud, gbpToUsd, gbpToEur, gbpToCad, gbpToAud, eurToGbp, eurToUsd2, eurToCad2, eurToAud2, eurToCad3, eurToAud3) => {
      eventbrite = {
        'GBP': new Platform("Eventbrite", "eventbrite", 0.065, 0.49, 9999999, paymentProcessingIncluded),
        'USD': new Platform("Eventbrite", "eventbrite", 0.035, 1.59, 9999999, platformPaymentProcessing['Eventbrite']['USD']),
        'EUR': new Platform("Eventbrite", "eventbrite", 0.05, 0.99, 9999999, paymentProcessingIncluded),
        'CAD': new Platform("Eventbrite", "eventbrite", 0.035, 0.99, 9999999, paymentProcessingIncluded),
        'AUD': new Platform("Eventbrite", "eventbrite", 0.05, 0.99, 9999999, paymentProcessingIncluded)
      };

      ticketTailor = {
        'GBP': new Platform("Ticket Tailor", "tickettailor", 0.00, 0.50, 9999999, platformPaymentProcessing['Stripe']['GBP']),
        'USD': new Platform("Ticket Tailor", "tickettailor", 0.00, 0.65, 9999999, platformPaymentProcessing['Ticket Tailor']['USD']),
        'EUR': new Platform("Ticket Tailor", "tickettailor", 0.00, 0.60, 9999999, platformPaymentProcessing['Stripe']['EUR']),
        'CAD': new Platform("Ticket Tailor", "tickettailor", 0.00, 0.90, 9999999, platformPaymentProcessing['Stripe']['CAD']),
        'AUD': new Platform("Ticket Tailor", "tickettailor", 0.00, 0.90, 9999999, platformPaymentProcessing['Stripe']['AUD'])
      };

      universe = {
        'GBP': new Platform("Universe", "universe", 0.055, 0.99, 9.95, paymentProcessingIncluded),
        'USD': new Platform("Universe", "universe", 0.025, 1.99, 19.95, platformPaymentProcessing['Stripe']['USD']),
        'EUR': new Platform("Universe", "universe", 0.045, 0.99, 19.95, paymentProcessingIncluded),
        'CAD': new Platform("Universe", "universe", 0.045, 0.59, 39.95, paymentProcessingIncluded),
        'AUD': new Platform("Universe", "universe", 0.05, 1.99, 9999999, paymentProcessingIncluded)
      };

      brownPaperTickets = {
        'GBP': new Platform("Brown Paper Tickets", "brownPaperTickets", 0.05, 0.79, 9999999, paymentProcessingIncluded),
        'USD': new Platform("Brown Paper Tickets", "brownPaperTickets", 0.05, 0.99, 9999999, paymentProcessingIncluded),
        'EUR': new Platform("Brown Paper Tickets", "brownPaperTickets", 0.05, 0.89, 999999, paymentProcessingIncluded),
        'CAD': new Platform("Brown Paper Tickets", "brownPaperTickets", 0.05, 0.99, 9999999, paymentProcessingIncluded),
        'AUD': new Platform("Brown Paper Tickets", "brownPaperTickets", 0.05, +usdToAud[0], 9999999, paymentProcessingIncluded)
      };

      ticketSource = {
        'GBP': new Platform("Ticket Source", "ticketSource", 0.045, 0.00, 9999999, platformPaymentProcessing['Stripe']['GBP']),
        'USD': new Platform("Ticket Source", "ticketSource", 0.045, 0.00, 9999999, platformPaymentProcessing['Stripe']['USD']),
        'EUR': new Platform("Ticket Source", "ticketSource", 0.045, 0.00, 9999999, platformPaymentProcessing['Stripe']['EUR']),
        'CAD': new Platform("Ticket Source", "ticketSource", 0.045, 0.00, 9999999, platformPaymentProcessing['Stripe']['CAD']),
        'AUD': new Platform("Ticket Source", "ticketSource", 0.045, 0.00, 9999999, platformPaymentProcessing['Stripe']['AUD'])
      };

      billetto = {
        'GBP': new Platform("Billetto", "billetto", 0.05, 0.6, 9.99, paymentProcessingIncluded),
        'USD': new Platform("Billetto", "billetto", 0.05, +eurToUsd[0], 9.99, paymentProcessingIncluded),
        'EUR': new Platform("Billetto", "billetto", 0.05, 0.5, 9.99, paymentProcessingIncluded),
        'CAD': new Platform("Billetto", "billetto", 0.05, +eurToCad[0], 9.99, paymentProcessingIncluded),
        'AUD': new Platform("Billetto", "billetto", 0.05, +eurToAud[0], 9.99, paymentProcessingIncluded)
      };

      tiTo = {
        'GBP': new Platform("Ti.to", "tiTo", 0.03, 0.00, 25, platformPaymentProcessing['Stripe']['GBP']),
        'USD': new Platform("Ti.to", "tiTo", 0.03, 0.00, 25, platformPaymentProcessing['Stripe']['USD']),
        'EUR': new Platform("Ti.to", "tiTo", 0.03, 0.00, 25, platformPaymentProcessing['Stripe']['EUR']),
        'CAD': new Platform("Ti.to", "tiTo", 0.03, 0.00, 25, platformPaymentProcessing['Stripe']['CAD']),
        'AUD': new Platform("Ti.to", "tiTo", 0.03, 0.00, 25, platformPaymentProcessing['Stripe']['AUD'])
      };

      skiddle = {
        'GBP': new Platform("Skiddle", "skiddle", 0.1, 0.25, 9999999, paymentProcessingIncluded),
        'USD': new Platform("Skiddle", "skiddle", 0.1, +gbpToUsd[0], 9999999, paymentProcessingIncluded),
        'EUR': new Platform("Skiddle", "skiddle", 0.1, +gbpToEur[0], 9999999, paymentProcessingIncluded),
        'CAD': new Platform("Skiddle", "skiddle", 0.1, +gbpToCad[0], 9999999, paymentProcessingIncluded),
        'AUD': new Platform("Skiddle", "skiddle", 0.1, +gbpToAud[0], 9999999, paymentProcessingIncluded)
      };

      eventix = {
        'GBP': new Platform("Eventix", "eventix", 0.00, +eurToGbp[0], 9999999, platformPaymentProcessing['Paypal']['GBP']),
        'USD': new Platform("Eventix", "eventix", 0.00, +eurToUsd2[0], 9999999, platformPaymentProcessing['Paypal']['USD']),
        'EUR': new Platform("Eventix", "eventix", 0.00, 1, 9999999, platformPaymentProcessing['Paypal']['EUR']),
        'CAD': new Platform("Eventix", "eventix", 0.00, +eurToCad2[0], 9999999, platformPaymentProcessing['Paypal']['CAD']),
        'AUD': new Platform("Eventix", "eventix", 0.00, +eurToAud2[0], 9999999, platformPaymentProcessing['Paypal']['AUD'])
      };

      xingEvents = {
        'GBP': new Platform("Xing Events", "xingEvents", 0.039, 0.89, 9999999, paymentProcessingIncluded),
        'USD': new Platform("Xing Events", "xingEvents", 0.039, 0.99, 9999999, paymentProcessingIncluded),
        'EUR': new Platform("Xing Events", "xingEvents", 0.039, 0.99, 9999999, paymentProcessingIncluded),
        'CAD': new Platform("Xing Events", "xingEvents", 0.039, +eurToCad3[0], 9999999, paymentProcessingIncluded),
        'AUD': new Platform("Xing Events", "xingEvents", 0.039, +eurToAud3[0], 9999999, paymentProcessingIncluded),
      };
    });
    //Get payment objects
    paymentProcessingIncluded = new PaymentProcessor("Payment processing fees are included in the service fees", 0.00, 0.00);
    platformPaymentProcessing = {
      'Eventbrite': {
        'USD': new PaymentProcessor("Payment processing fees", 0.025, 0.00)
      },
      'Ticket Tailor': {
        'USD': new PaymentProcessor("Payment processing fees", 0.025, 0.00)
      },
      'Stripe': {
        'GBP': new PaymentProcessor("Payment processing fees", 0.014, 0.2),
        'USD': new PaymentProcessor("Payment processing fees", 0.029, 0.3),
        'EUR': new PaymentProcessor("Payment processing fees", 0.014, 0.25),
        'CAD': new PaymentProcessor("Payment processing fees", 0.029, 0.3),
        'AUD': new PaymentProcessor("Payment processing fees", 0.0175, 0.3)
      },
      'Paypal': {
        'GBP': new PaymentProcessor("Payment processing fees", 0.014, 0.3),
        'USD': new PaymentProcessor("Payment processing fees", 0.029, 0.3),
        'EUR': new PaymentProcessor("Payment processing fees", 0.029, 0.35),
        'CAD': new PaymentProcessor("Payment processing fees", 0.029, 0.3),
        'AUD': new PaymentProcessor("Payment processing fees", 0.026, 0.3)
      }
    };
    //Masonry
    var $grid = $('.masonry-grid').imagesLoaded(function () {
      // init Masonry after all images have loaded
      $grid.masonry({
        itemSelector: '.masonry-grid__item',
        gutter: 30,
        percentPosition: true
      });
    });
    //Tooltip
    $('.tip').tipr({
      mode: 'above'
    });
    //Range
    priceRange.slider({
      min: $('.price-range').parent().find('.range__min').data('price-min'),
      max: $('.price-range').parent().find('.range__max').data('price-max'),
      value: $('.price-range').data('price-current'),
      create: function () {
        modifiedPriceValue = numberWithCommas($(this).slider('value'));
        priceHandle.attr('data-value', getCurrencySymbol(currencySymbols, currencyName) + modifiedPriceValue);
        $('.inputs__price').find('input').val(modifiedPriceValue);
      }
    });
    priceRange.on('slide', function (event, ui) {
      modifiedPriceValue = numberWithCommas(ui.value);
      priceHandle.attr('data-value', getCurrencySymbol(currencySymbols, currencyName) + modifiedPriceValue);
      $('.inputs__price').find('input').val(modifiedPriceValue);
    });
    ticketsRange.slider({
      min: $('.sales-range .range__min').data('sales-min'),
      max: $('.sales-range .range__max').data('sales-max'),
      value: $('.tickets-range').data('sales-current'),
      create: function () {
        modifiedTicketsValue = numberWithCommas($(this).slider('value'));
        ticketsHandle.attr('data-value', modifiedTicketsValue);
        $('.inputs__tickets').find('input').val(modifiedTicketsValue);
      }
    });
    ticketsRange.on('slide', function (event, ui) {
      modifiedTicketsValue = numberWithCommas(ui.value);
      ticketsHandle.attr('data-value', modifiedTicketsValue);
      $('.inputs__tickets').find('input').val(modifiedTicketsValue);
    });
    //Blog ajax
    $('.blog-template__btn').on('click', function () {
      var pageValue = $(this).data('page');
      var pageMaxValue = $(this).data('max-page');
      var button = $(this);
      $.ajax({
        url: ajax.url,
        data: {
          'action': 'loadmore',
          'page': pageValue
        },
        type: 'POST',
        beforeSend: function () {
          button.text('Loading...');
        },
        success: function (results) {
          if (results) {
            button.attr('data-page', pageValue + 1);
            $content = $(results);
            $('.masonry-grid').append($content).masonry('appended', $content);;
            if (pageValue + 1 === pageMaxValue) {
              button.hide();
            } else {
              button.text('Load more articles');
            }
          }
        }
      });
    });
    //Math scripts
    $(document).on('input', '.inputs input', function () {
      var value = $(this).val();
      var name = $(this).attr('name');
      switch (name) {
        case 'price-value':
          var priceRangeInstance = getRangeSliderInstance(priceRange);
          var maxValue = priceRangeInstance.options.max;
          var minValue = priceRangeInstance.options.min;
          if (value > maxValue) {
            priceRange.slider('value', maxValue);
            $('.price-handle').attr('data-value', getCurrencySymbol(currencySymbols, currencyName) + maxValue);
            $(this).val(maxValue);
          } else if (value < minValue) {
            priceRange.slider('value', minValue);
            $('.price-handle').attr('data-value', getCurrencySymbol(currencySymbols, currencyName) + minValue);
            $(this).val(minValue);
          } else if (value === '') {
            priceRange.slider('value', 0);
            $('.price-handle').attr('data-value', getCurrencySymbol(currencySymbols, currencyName) + 0);
            $(this).val('');
          } else {
            priceRange.slider('value', value);
            $('.price-handle').attr('data-value', getCurrencySymbol(currencySymbols, currencyName) + value);
            $(this).val(value);
          }
          break;
        case 'tickets-value':
          var ticketsRangeInstance = getRangeSliderInstance(ticketsRange);
          var maxValue = ticketsRangeInstance.options.max;
          var minValue = ticketsRangeInstance.options.min;
          if (value > maxValue) {
            ticketsRange.slider('value', maxValue);
            $('.tickets-handle').attr('data-value', maxValue);
            $(this).val(maxValue);
          } else if (value < minValue) {
            ticketsRange.slider('value', minValue);
            $('.tickets-handle').attr('data-value', minValue);
            $(this).val(minValue);
          } else if (value === '') {
            ticketsRange.slider('value', 0);
            $('.tickets-handle').attr('data-value', 0);
            $(this).val('');
          } else {
            ticketsRange.slider('value', value);
            $('.tickets-handle').attr('data-value', value);
            $(this).val(value);
          }
          break;
        default:
          return;
      }
    });
    //Select
    $('.inputs__select select').on('change', function () {
      var selectedValue = $(this).val();
      currencyName = selectedValue.toUpperCase();
      var currencySymbol = getCurrencySymbol(currencySymbols, currencyName);
      changeCurrencySymbol(currencySymbol);
    });
    //Serialize
    $('.math-form__btn').on('click', function (e) {
      e.preventDefault();
      $(this).css('opacity', .4);
      var formdata = $(this).closest('form').serializeArray();
      var data = {};
      $(formdata).each(function (index, obj) {
        data[obj.name] = obj.value;
      });
      mainPlatform = data['platform'];
      var result = calculateFee(data['price-value'], data['price-currency'], data['tickets-value']);
      setTimeout(function () {
        $('.math-form__btn').css('opacity', 1);
        $('html, body').animate({
          scrollTop: $('.table-section').offset().top
        }, 500);
        if ($('.table-section__grid').hasClass('table-section__grid_opened')) {} else {
          $('.table-section__grid').addClass('table-section__grid_opened');
        }
      }, 500);
      result.sort(function (a, b) {
        return a.total - b.total;
      });
      result.forEach(function (item, index, array) {
        if (mainPlatform === item.name) {
          primaryIndex = index;
        }
      });
      var sortedArray = move(result, primaryIndex, 0);
      showResults(sortedArray);
    });
  });
}(jQuery);